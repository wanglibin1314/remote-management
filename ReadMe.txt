功能：远程桌面 + Radmin + SSH 管理
注意：可执行文件需要splite3.dll才能运行

编译器: VC2010


20201116:	V0.0
新建工程
完成主界面

20201120:	V1.0
完成基本操作,但还缺少以下功能待完成：
1. 分组导入、导出，移动主机到别的分组
2. 在线检测、网络扫描
3. 开机密码功能、密码加密存储
4. 添加VNC主机控制功能

20201121:
1. 完成列表框数据的拖放来改变分组，同时删除右键菜单的移动到分组
2. 分组重命名操作
3. 添加树节点左边的+-号
4. 添加LICENSE文件，避免Gitee打扰

20201122:
1. 修正远程桌面密码为空时不能直接打开
2. 加入开机密码校验功能，对开机密码AES加密, 密码最大16字节
3. 主机密码AES加密存储, 密码最大16字节

20201122_1: 
1. 添加记住软件关闭前打开的分组功能

20201122_2:
1. 修改命令行，当主机端口为默认时，命令行中不添加端口号
2. 版本修改为1.1

20201123:
1. 树控件总是显示选择行
2. 修正远程桌面密码为空时还是不能直接打开，需要记住凭据后才能在下次直接打开
3. 去除用处不大的网络扫描按钮


